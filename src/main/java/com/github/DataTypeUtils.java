package com.github;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;

import lombok.experimental.UtilityClass;

/**
 * @author lijinting01
 *
 */
@UtilityClass
public class DataTypeUtils {

	/**
	 * @param o
	 * @return
	 */
	public static double toDouble(Object o) {
		if (o == null)
			return 0L;
		if (o instanceof Number) {
			return ((Number) o).doubleValue();
		} else if (o.getClass().isPrimitive()) {
			return (double) o;
		}
		return 0L;
	}

	/**
	 * @param o
	 * @return
	 */
	public static boolean toBoolean(Object o) {
		if (o == null)
			return false;
		if (o.getClass().isAssignableFrom(Number.class)) {
			return ((Number) o).intValue() != 0L;
		} else if (o.getClass().isPrimitive()) {
			return (int) o != 0L;
		} else if (o.getClass() == String.class) {
			return StringUtils.isNotEmpty((String) o);
		}
		return false;
	}

	public static boolean isBigNumber(Class<?> c) {
		return c == BigDecimal.class || c == BigInteger.class;
	}

	public static boolean isInteger(Class<?> c) {
		return isWrappedInteger(c) || isPrimitiveInteger(c);
	}

	public static boolean isDecimal(Class<?> c) {
		return isPrimitiveDecimal(c) || isWrappedDecimal(c);
	}

	public static boolean isWrappedInteger(Class<?> c) {
		return c == Integer.class || c == Short.class || c == Long.class || c == Byte.class;
	}

	public static boolean isPrimitiveInteger(Class<?> c) {
		return c == int.class || c == long.class || c == short.class || c == byte.class;
	}

	public static boolean isPrimitiveDecimal(Class<?> c) {
		return c == float.class || c == double.class;
	}

	public static boolean isWrappedDecimal(Class<?> c) {
		return c == Float.class || c == Double.class;
	}

}
