package com.github;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * title文字和index的对照表
 * 
 * @author Justin
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class Title {
	private String titleText;
	private int titleIndex;
}
