package com.github;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Excel中单个sheet中的单元格
 * 
 * @author Justin
 *
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface CellAt {
	/**
	 * 单元格的 cellIndex，从0开始计数。 如果为-1则表示当前属性无效，应当予以忽略。
	 * 
	 * @return
	 */
	int index() default -1;

	/**
	 * <pre>
	 * 单元格对应的title。适用于通过标题行定位所要取得单元格的情况。 如果为空字符串或null则表示当前属性无效，应当予以忽略。
	 * 如果写入时没有设置【title】属性，则不会写入标题行。
	 * </pre>
	 * 
	 * @return
	 */
	String title() default "";

	/**
	 * <pre>
	 * index和title同时配置时，优先使用index进行读取。仅在读取时该选项有效，写入时只写入Title。如果Title缺失则不写入标题行。
	 * </pre>
	 * 
	 * @return
	 */
	boolean useIndexPrior() default true;
}
