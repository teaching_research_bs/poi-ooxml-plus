package com.github;

/**
 * 
 * @author Justin
 *
 */
public class WorkbookCreationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3435852173399109319L;

	/**
	 * WorkbookCreationException
	 */
	public WorkbookCreationException() {
		super();
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public WorkbookCreationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public WorkbookCreationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param message
	 */
	public WorkbookCreationException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param cause
	 */
	public WorkbookCreationException(Throwable cause) {
		super(cause);
	}

}
