package com.github;

import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * workbook提供者
 * 
 * @author lijinting01
 *
 */
@FunctionalInterface
public interface WorkbookProvider {

	/**
	 * @return
	 * @throws IOException
	 */
	Workbook provide() throws IOException;
}
