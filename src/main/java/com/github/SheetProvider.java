package com.github;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * @author lijinting01
 *
 */
@FunctionalInterface
public interface SheetProvider {
	/**
	 * @return
	 */
	Sheet provide();
}
