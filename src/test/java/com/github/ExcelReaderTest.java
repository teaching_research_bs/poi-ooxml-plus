package com.github;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.java.Log;

@Log
public class ExcelReaderTest {

	private ExcelReader excelHelper;

	@Before
	public void setUp() throws Exception {
		InputStream is = ClassLoader.getSystemResourceAsStream("623JD.xlsx");
		excelHelper = ExcelReader.builder(is).title(0).beginRow(1).endRow(300).build();
		assertThat(excelHelper, is(notNullValue()));
	}

	@Test
	public void testRead1() {
		List<TestBean1> list = excelHelper.read(TestBean1.class, "订单");
		assertThat(list, is(notNullValue()));
		assertThat(list.size(), is(equalTo(299)));
		log.info(list.toString());
	}

	@Test
	public void testRead2() {
		List<TestBean2> list = excelHelper.read(TestBean2.class, 2);
		assertThat(list, is(notNullValue()));
		assertThat(list.size(), is(equalTo(299)));
		log.info(list.toString());
	}

	/**
	 * 没有设定合适的页和标题行。期待取得的结果都是初始值。
	 */
	@Test
	public void testWrongFileInput() {
		InputStream is = ClassLoader.getSystemResourceAsStream("623JD.xlsx");
		ExcelReader wrong = ExcelReader.builder(is).title(0).beginRow(1).endRow(300).build();
		List<TestBean1> list = wrong.read(TestBean1.class, "订单");
		assertThat(list, is(notNullValue()));
		log.info(list.toString());
	}

}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean1 {
	/**
	 * 
	 */
	@CellAt(title = "辐射区域")
	private String area;

	@CellAt(title = "商品编码")
	private String model;

	@CellAt(title = "入库机构")
	private String barnName;

	@CellAt(title = "订单数量")
	private int quantity;

	@CellAt(title = "单位体积")
	private Double unitVolumn;
}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean2 {
	/**
	 * 
	 */
	@CellAt(index = 0)
	private String field0;

	@CellAt(index = 1)
	private Integer field1;

	@CellAt(index = 2)
	private String field2;

	@CellAt(index = 3)
	private String field3;

	@CellAt(index = 4)
	private String field4;

}