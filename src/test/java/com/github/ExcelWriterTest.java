package com.github;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Cleanup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;

public class ExcelWriterTest {

	@Test
	@SneakyThrows
	public void testCreateSheet3() {

		TestBean3 bean3 = TestBean3.builder().field0(RandomStringUtils.randomNumeric(8)).field1(0).field2(1.0)
				.field3(BigDecimal.ONE).field4(Boolean.FALSE).field5(10L).build();
		@Cleanup
		Workbook wb = new XSSFWorkbook();
		ExcelWriter writer = ExcelWriter.builder().beginColumn(0).beginRow(1).rowCount(10).workbook(wb).build();
		writer.write("new sheet", Arrays.asList(bean3), TestBean3.class);
		ExcelReader reader = ExcelReader.builder(() -> writer.getWorkbook()).title(0).beginRow(1).endRow(10).build();
		final List<TestBean3> list = reader.read(TestBean3.class, 0);
		assertThat(list.size()).isEqualTo(1);
		final TestBean3 newBean3 = list.get(0);
		assertThat(newBean3.getField0()).isEqualTo(bean3.getField0());
		assertThat(newBean3.getField1()).isEqualTo(bean3.getField1());
		assertThat(newBean3.getField2()).isEqualTo(bean3.getField2());
		assertThat(newBean3.getField3().subtract(bean3.getField3()).compareTo(BigDecimal.ZERO)).isEqualTo(0);
		assertThat(newBean3.getField4()).isEqualTo(bean3.getField4());
	}

	@Test
	@SneakyThrows
	public void testCreateSheet4() {
		TestBean4 bean4 = TestBean4.builder().field0(RandomStringUtils.randomNumeric(8)).field1(0).field2(1.0)
				.field3(BigDecimal.ONE).field4(Boolean.FALSE).field5(10L).build();
		ExcelWriter writer = ExcelWriter.builder().titleRow(0).beginColumn(2).beginRow(1).rowCount(10)
				.workbook(new XSSFWorkbook()).build();
		Workbook wb = writer.write("new sheet", Arrays.asList(bean4), TestBean4.class).getWorkbook();

		ExcelReader reader = ExcelReader.builder(() -> wb).beginRow(1).endRow(10).build();
		final List<TestBean4> list = reader.read(TestBean4.class, "new sheet");
		assertThat(list.size()).isEqualTo(1);
		final TestBean4 newBean4 = list.get(0);
		assertThat(newBean4.getField0()).isEqualTo(bean4.getField0());
		assertThat(newBean4.getField1()).isEqualTo(bean4.getField1());
		assertThat(newBean4.getField2()).isEqualTo(bean4.getField2());
		assertThat(newBean4.getField3().subtract(bean4.getField3()).compareTo(BigDecimal.ZERO)).isEqualTo(0);
		assertThat(newBean4.getField4()).isEqualTo(bean4.getField4());
	}

}

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean3 {
	@CellAt(index = 0)
	private String field0;

	@CellAt(index = 1)
	private Integer field1;

	@CellAt(index = 2)
	private Double field2;

	@CellAt(index = 3)
	private BigDecimal field3;

	@CellAt(index = 4)
	private Boolean field4;

	@CellAt(index = 5)
	private long field5;
}

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean4 {
	@CellAt(title = "0", useIndexPrior = false)
	private String field0;

	@CellAt(title = "1", useIndexPrior = false)
	private Integer field1;

	@CellAt(title = "2", useIndexPrior = false)
	private Double field2;

	@CellAt(title = "3", useIndexPrior = false)
	private BigDecimal field3;

	@CellAt(title = "4", useIndexPrior = false)
	private Boolean field4;

	@CellAt(title = "5", useIndexPrior = false)
	private long field5;
}