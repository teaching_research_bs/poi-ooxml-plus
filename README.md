#poi-ooxml-plus
基于Apache POI开发的库，让开发者更方便地将Excel元素映射成对象。

## 使用方法
### 定义一个RowBean
```
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean1 {
    @CellAt(title = "序号") // 注解对应的标题列
    private String field0;

    @CellAt(title = "条码")
    private String field1;

    @CellAt(title = "商品名称")
    private String field2;

    @CellAt(title = "单位")
    private String field3;

    @CellAt(title = "售价")
    private BigDecimal field4;
}
```

```
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TestBean2 {
    @CellAt(index = 0)
    private String field0; // 或者注解对应的标题index

    @CellAt(index = 1)
    private String field1;
}
```

### 创建Workbook

```
InputStream is = ...;
ExcelReader excelReader = ExcelReader.builder(is)
                        .title(0) // 设定标题是哪一行。默认为0
                        .begin(1) // 设定从哪一行开始读内容，默认为1
                        .end(300) // 设定最多读到哪一行。
                        .build();
```

### 从Workbook读出数据并转换成 RowBean
```
List<TestBean2> list = excelReader.read(TestBean2.class, 0); //读第0个sheet，并转换成RowBean
```
### 将RowBean写入Excel
```
TestBean4 bean4 = TestBean4.builder().field0(RandomStringUtils.randomNumeric(8)).field1(0).field2(1.0)
				.field3(BigDecimal.ONE).field4(Boolean.FALSE).field5(10L).build();
		ExcelWriter writer = ExcelWriter.builder().titleRow(0).beginColumn(2).beginRow(1).rowCount(10)
				.workbook(new XSSFWorkbook()).build();
		Workbook wb = writer.write("new sheet", Arrays.asList(bean4), TestBean4.class).getWorkbook();
```

### 选择合适的Workbook
POI的Workbook有两个实现——XSSFWorkbook和HSSFWorkbook。ExcelReader/ExcelWriter 默认使用XSSFWorkbook，也就是解析xlsx后缀的EXCEL表格。
如果需要使用HSSFWorkbook，修改Builder的Workbook属性即可。

```
InputStream ins = ...;
ExcelWorkbook.Builder builder = ExcelWorkbook.builder(ins).begin(1).title(0).end(1000);
if (StringUtils.endsWith(file.getOriginalFilename(), ".xls")) {
    builder.workbookProvider((InputStream is) -> new HSSFWorkbook(is));
}
```


